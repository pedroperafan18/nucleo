<!DOCTYPE html>
<!--Creacion de WhiskeyPic -->
<!--Expertos en Diseño y Programación Web -->
<!--Contacto: pedroperafan18@gmail.com, lge_valdez@hotmail.com, gerardo.serna.0310@gmail.com, cristobal_llamas@hotmail.com -->
<!--Creacion de WhiskeyPic -->
<html lang="es">
  <?php if(isset($head)) echo $head;?>
  <body id="body">
    <?php
    if($this->uri->segment(1)!="admin"):
    ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <article class="loader">
      <div class="load">
        <img src="<?=base_url("img/ico.png")?>"/>
      </div>
    </article>

    	<div class="fixed-action-btn horizontal click-to-toggle" style="bottom: 45px; right: 24px;">
		    <a href="<?php echo base_url("buscador");?>" class="btn-floating btn-large  red darken-4">
		      <i class="fa fa-search"></i>
		    </a>
		  </div>
    <div class="fixed-action-btn horizontal" style="bottom: 120px; right: 24px;">
    <a class="btn-floating btn-large  red darken-4">
    <i class="fa fa-paint-brush"></i>
    </a>
    <ul>
    <li><a class="btn-floating orange accent-4 theme" data-theme-color="naranja"></a></li>
    <li><a class="btn-floating light-green accent-4 theme" data-theme-color="verde"></a></li>
    <li><a class="btn-floating pink darken-1 theme" data-theme-color="magneta"></a></li>
    <li><a class="btn-floating cyan darken-1 theme" data-theme-color="cyan"></a></li>
    </ul>
    </div>
    <?php endif;?>
    <?php if(isset($menu)) echo $menu;?>
    <?php if(isset($contenido)) echo $contenido;?>
    <?php if(isset($footer)) echo $footer;?>
    <?php if(isset($javascript)) echo $javascript;?>
    <script>
    $(document).ready(function(){$('.theme').click(function(event){var theme = $(this).data('theme-color');
    $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href',url+'css/temas/'+theme+'.css'));});});
    </script>
    <!--Creacion de WhiskeyPic -->
    <!--Expertos en Diseño y Programación Web -->
    <!--Contacto: pedroperafan18@gmail.com, lge_valdez@hotmail.com, gerardo.serna.0310@gmail.com, cristobal_llamas@hotmail.com -->
  </body>
</html>