<head>
    <meta charset="utf-8">
    <meta name="description" content="<?=$descripcion?>">
    <meta name="keywords"    content="<?php if(isset($keywords)){echo $keywords;}?>">
    <meta name="viewport"    content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author"      content="City En Linea">

    <meta property="og:title"              content="<?=$titulo?>"/>
    <meta property="og:url"                content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" />
    <meta property="og:type"               content="website"/>
    <meta property="og:description"        content="<?=$descripcion?>"/>
    <meta property="og:image"              content="<?php if(isset($ico)){echo $ico;}?>"/>
    <meta http-equiv="X-UA-Compatible"     content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$titulo?></title>
    <?php foreach ($css as $key =>$e){echo '<link href="'.base_url().'css/'.$e.'.css" rel="stylesheet">';}?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php if(isset($ico)){echo $ico;}?>" type="image/png">
</head>