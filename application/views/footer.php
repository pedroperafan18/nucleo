<!-- FOOTER -->
<div class="row">
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l4">
                    <a class="twitter-timeline" href="https://twitter.com/cityslp" data-widget-id="717926261926993920">
                        Tweets por el @cityslp.
                    </a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                    </script>
                </div>
                <div class="col l4">
                    <div class="container">
                    <h5 class="white-text"><i class="fa fa-map-signs"></i> Secciones</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="<?=base_url("contacto")?>"><i class="fa fa-caret-right"></i> Registro</a></li>
                        <li><a class="grey-text text-lighten-3" href="<?=base_url("directorio")?>"><i class="fa fa-caret-right"></i> Directorio</a></li>
                        <li><a class="grey-text text-lighten-3" href="<?=base_url("contacto")?>"><i class="fa fa-caret-right"></i> Agenda tu Evento</a></li>
                        <li><a class="grey-text text-lighten-3" href="<?=base_url("privacidad")?>"><i class="fa fa-caret-right"></i> Aviso de Privacidad</a></li>
                    </ul>
                    </div>
                </div>

                 <div class="col l4">
                    <h5 class="white-text"><i class="fa fa-comment"></i> Contacto</h5>
                    <p class="grey-text text-lighten-4"><i class="fa fa-map-marker"></i> Vista Hermosa N.155, San Luis Potosí, S.L.P.</p>
                    <p class="grey-text text-lighten-4"><i class="fa fa-phone"></i> 01 444 812 26 47</p>
                    <p class="grey-text text-lighten-4"><i class="fa fa-paper-plane"></i> agendatuevento@cityenlinea.com</p>
                    <p class="grey-text text-lighten-4"><i class="fa fa-paper-plane"></i> contacto@cityenlinea.com</p>
                </div>
               
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">City Life &amp; Style de San Luis Potosí © <?=date('Y')?>. Todos los Derechos Reservados</div>
        </div>
    </footer>
</div>