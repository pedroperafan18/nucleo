<div class="navbar-fixed">
    <nav>
        <!-- MENU MOBIL-->
		<ul id="slide-out" class="side-nav">
            <li><a href="<?=base_url("Inicio")?>">Inicio</a></li>
			<li class="no-padding">
				<ul class="collapsible collapsible-accordion">
					<li>
						<a href="<?=base_url("eventos")?>" class="collapsible-header">Eventos<i class="mdi-navigation-arrow-drop-down"></i></a>
						<div class="collapsible-body">
							<ul>
                                <li><a href="<?=base_url("buscador")?>">Busca tu Evento</a></li>
				                <li><a href="<?=base_url("eventos/sociales")?>">Eventos Sociales</a></li>
                                <li><a href="<?=base_url("eventos/restaurantes")?>">Bares, Antros y Restaurantes</a></li>
                        	</ul>
						</div>
					</li>
				</ul>
			</li>
            <li><a href="<?=base_url("columnas")?>">Blog</a></li>
			<li class="no-padding">
				<ul class="collapsible collapsible-accordion">
					<li>
						<a href="#" class="collapsible-header">Secciones<i class="mdi-navigation-arrow-drop-down"></i></a>
						<div class="collapsible-body">
							<ul>
								<li><a href="<?=base_url("secciones/1")?>">Look</a></li>
                                <li><a href="<?=base_url("secciones/2")?>">Viajero</a></li>
                                <li><a href="<?=base_url("secciones/3")?>">Con Gusto</a></li>
                                <li><a href="<?=base_url("secciones/4")?>">La Sesión</a></li>
                                <li><a href="<?=base_url("secciones/5")?>">Quien Soy</a></li>
                                <li><a href="<?=base_url("secciones/6")?>">Portada Sustancial</a></li>
                                <li><a href="<?=base_url("secciones/7")?>">Cocina De</a></li>
                                <li><a href="<?=base_url("secciones/8")?>">Acciones</a></li>
                                <li><a href="<?=base_url("secciones/9")?>">Salud</a></li>
                                <li><a href="<?=base_url("secciones/10")?>">Ciudadano Activo</a></li>
                                <li><a href="<?=base_url("secciones/11")?>">Figura</a></li>
                                <li><a href="<?=base_url("secciones/12")?>">Cultura</a></li>
                        	</ul>
						</div>
					</li>
				</ul>
			</li>
            <li><a href="<?=base_url("revista")?>">Revista Impresa</a></li>
			<li><a href="<?=base_url("contacto")?>">Contacto</a></li>
            <li><a href="https://www.facebook.com/Cityenlinea" target="_blank">Facebook</a></li>
			<li><a href="https://www.instagram.com/cityslp" target="_blank">Instagram</a></li>
            <li><a href="https://twitter.com/cityslp" target="_blank">Twitter</a></li>
            <li><a href="https://es.pinterest.com/revistacityslp" target="_blank">Pinterest</a></li>
            <li><a href="https://www.youtube.com/channel/UCbieK3LTXK06C3LDcsuevkQ" target="_blank">Youtube</a></li>
		</ul>
   
        <!-- MENU DESKTOP-->
		<ul class="left hide-on-med-and-down" id="menu-list">
            <li class="logo center"><a href="<?=base_url()?>"><img src="<?=base_url('img/logo-white.png')?>"></a></li>
            <li class="brand-logo hide-on-large-only center"><a href="<?=base_url()?>"><img src="<?=base_url('img/logo-white.png')?>"></a></li>
            <li><a class="dropdown-button" href="<?=base_url("eventos")?>" data-activates="dropdown1">Eventos</a></li>
				<ul id='dropdown1' class='dropdown-content drop-menu'>
                    <li class="more"><a href="<?=base_url("buscador")?>">Busca tu Evento</a></li>
					<li class="more"><a href="<?=base_url("eventos/sociales")?>">Eventos Sociales</a></li>
                    <li class="more"><a href="<?=base_url("eventos/restaurantes")?>">Bares, Antros y Restaurantes</a></li>
				</ul>
            <li><a href="<?=base_url("calendario")?>">Calendario</a></li>
            <li><a href="<?=base_url("columnas")?>">Blog</a></li>
			<li><a class="dropdown-button" href="#" data-activates="dropdown2">Secciones <i class="mdi-navigation-arrow-drop-down"></i></a></li>
				<ul id='dropdown2' class='dropdown-content'>
					<li><a href="<?=base_url("secciones/1")?>">Look</a></li>
                    <li><a href="<?=base_url("secciones/2")?>">Viajero</a></li>
                    <li><a href="<?=base_url("secciones/3")?>">Con Gusto</a></li>
                    <li><a href="<?=base_url("secciones/4")?>">La Sesión</a></li>
                    <li><a href="<?=base_url("secciones/5")?>">Quien Soy</a></li>
                    <li><a href="<?=base_url("secciones/6")?>">Portada Sustancial</a></li>
                    <li><a href="<?=base_url("secciones/7")?>">Cocina De</a></li>
                    <li><a href="<?=base_url("secciones/8")?>">Acciones</a></li>
                    <li><a href="<?=base_url("secciones/9")?>">Salud</a></li>
                    <li><a href="<?=base_url("secciones/10")?>">Ciudadano Activo</a></li>
                    <li><a href="<?=base_url("secciones/11")?>">Figura</a></li>
                    <li><a href="<?=base_url("secciones/12")?>">Cultura</a></li>
				</ul>
            <li><a href="<?=base_url("revista")?>">Revista</a></li>
			<li><a href="<?=base_url("contacto")?>">Contacto</a></li>
		</ul>
        <ul class="right hide-on-med-and-down">
			<li class="social"><a href="https://www.facebook.com/Cityenlinea" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li class="social"><a href="https://es.pinterest.com/revistacityslp" target="_blank" class="pinterest"><i class="fa fa-pinterest-p"></i></a></li>
            <li class="social"><a href="https://www.instagram.com/cityslp" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a></li>
            <li class="social"><a href="https://twitter.com/cityslp" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li class="social">
                <a href="https://www.youtube.com/channel/UCbieK3LTXK06C3LDcsuevkQ" target="_blank" class="youtube tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver nuestros videos"><i class="fa fa-youtube-play"></i></a>
            </li>
		</ul>
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
	</nav>
</div>