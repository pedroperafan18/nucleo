 <style>
body{
background:#fafafa;
}
.barra-lateral{
background:#263238;
}
.logo{
margin: 10px 0 10px 0;
background:#fff;
border-radius: 3px;
}
.logo img{
width:50%;
margin: 10px auto 10px auto;
display: table;
}
.cont:first-of-type{
display:block;
}
.cont{
display:none;
}
.titulo{
width: 100%;
margin: 15px 0 15px 0;
padding: 5px 20px 5px 20px;
font-size: 28px;
color: #fff;
display:table;
background: #263238;
border-radius: 3px;
font-weight:100;
}
.cont:first-of-type{
display:block;
}
.cont{
display:none;
}
.titulo{
width: 100%;
margin: 15px 0 15px 0;
padding: 5px 20px 5px 20px;
font-size: 28px;
color: #fff;
display:table;
background: #263238;
border-radius: 3px;
font-weight:100;
}
.thumb {
height: 100px;
border: 1px solid #000;
margin: 10px 5px 0 0;
}
.collection-item:hover{
    background: #64b5f6 !important;
    color:#fff !important;
}
.collection-item{
    color:#64b5f6 !important;

}
</style>
<!--
<a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>
-->
<!-- Modal Structure -->
<div id="ModalEliminar" class="modal">
    <div class="modal-content">
        <h4>¿Está seguro de esta opción?</h4>
        <div class="hide" id="ModalAccion"></div>
        <div class="hide" id="ModalElemento"></div>
    </div>
    <div class="modal-footer">
        <a  href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
        <a id="confirmar" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Guardar cambios</a>
    </div>
</div>
<div class="row">
    <div id="menu-lateral" class="col s12 m4 l3 ">
        <br>
        <br>
        <div class="row">
            <div class="col s6 push-s3 m6 push-m3 l6 push-l3">
                <img class="responsive-img" src="<?php echo base_url("img/Ivateclogo.png");?>">
            </div>
            <div class="col s12 m12 l12">
                <div class="collection">
                    <a href="#!" data-action="s1" class="collection-item">Dashboard</a>
                    <a href="#!" data-action="s2" class="collection-item">Tareas</a>
                    <a href="#!" data-action="s3" class="collection-item">Mensajes</a>
                    <a href="#!" data-action="s4" class="collection-item">Proyectos</a>
                    <a href="#!" data-action="s5" class="collection-item">Estadisticas</a>
                    <a href="#!" data-action="s6" class="collection-item">Configuraciones</a>
                    <a href="<?= base_url("admin/logout")?>" class="collection-item">Salir</a>
                </div>
            </div>
        </div>
    </div>

    <div id="dashboard" class="col s12 m8 l9 blue lighten-1" style="min-height: 1000px">
        <!-- CONTENIDO-->
        <div id="s1" class="row cont">
            <div class="col s12 m12 l12 contenido">
                <label class="titulo">Dashboard</label>
                <div class="col s10 push-s1 white">
                    <h4>¡Bienvenido!</h4>
                </div>
            </div>
        </div>

        <div id="s2" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Tareas</label>
                <button class="col s12 m12 l2 btn red teal accent-4 right" type="button" data-action="ss2">Nuevo</button>
                <table class="responsive-table centered white">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Fecha </th>
                            <th>Importancia</th>
                            <th>Tipo</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($eventos) && !empty($eventos)){
                        foreach($eventos as $key =>$value){
                        if($value['TipoEvento']==1){$value['TipoEvento']="Frontend";}else{
                        if($value['TipoEvento']==2){$value['TipoEvento']="Backend";}}
                        echo '
                        <tr id="evento-'.$value["ID"].'">
                            <td>'.$value["ID"].'</td>
                            <td>'.$value["Nombre"].'</td>
                            <td>'.$value["Fecha"].'</td>
                            <td>
                                <form method="post" action="">
                                    <div data-id="evento-'.$value["ID"].'" data-imp="evento-'.$value["Importancia"].'" class="subir waves-effect waves-light btn">
                                        <i class="fa fa-arrow-up"></i>
                                    </div>
                                    <div data-id="evento-'.$value["ID"].'" data-imp="evento-'.$value["Importancia"].'" class="bajar waves-effect waves-light btn">
                                        <i class="fa fa-arrow-down"></i>
                                    </div>
                                </form>
                            </td>
                            <td>'.$value["TipoEvento"].'</td>
                            <td><a class="btn-flat eliminarevento" data-id="'.$value["ID"].'"><i class="fa fa-trash"></i></a></td>
                            <td data-id="evento-'.$value["ID"].'" value="'.$eventos[$key]["ID"].'"><a class="btn-flat datoseditarevento datosfotos" data-id="'.$value["ID"].'" data-action="sss2"><i class="fa fa-pencil"></i></a></td>
                        </tr>';
                        }
                        }else{
                        echo null;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="ss2" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Registrar Tarea</label>
                <form class="col s12 white" style="margin:20px 0 20px 0;" method="post" action="<?php echo base_url("admin/evento/nuevo")?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="Evento" id="NombreEvento" class="validate" required>
                            <label for="Evento">Nombre del Empleado</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="date" name="Fecha" id="FechaEvento" class="datepicker" required>
                            <label for="Fecha">Fecha de Entrega</label>
                        </div>
                    </div>
                    <div class="row" style="display: none">
                        <div class="input-field col s12">
                            <input type="text" name="Lugar" id="LugarEvento" class="validate" required>
                            <label for="Lugar">Lugar del evento</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="TipoEvento" id="TipoEvento" required>
                                <option value="" disabled selected>Selecciona tipo de tarea</option>
                                <option value="1">Frontend</option>
                                <option value="2">Backend</option>
                            </select>
                            <label>Tipo de tarea</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Fotografí<as></as></span>
                                <input type="file" id="files" name="userfile[]"  multiple required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <output id="list"></output>
                        </div>
                    </div>
                    <button class="btn red darken-2 waves-effect waves-light right" type="submit">Registrar</button>
                </form>
            </div>
        </div>
        <div id="sss2" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Actualizar Tarea</label>
                <form class="col s12" style="margin:20px 0 20px 0;" method="post" action="" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="id-e" value="" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="Evento" id="NombreEventoA" class="validate" required>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="date" name="Fecha" id="FechaEventoA" class="datepicker" required>
                            
                        </div>
                    </div>
                    <div class="row" style="display: none">
                        <div class="input-field col s12">
                            <input type="text" name="Lugar" id="LugarEventoA" class="validate" required>
                            
                        </div>
                    </div>
                    <!--
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="Importancia">
                                <option selected disabled>Seleccione la importancia</option>
                                <option value="1">Poca</option>
                                <option value="2">Normal</option>
                                <option value="3">Alta</option>
                            </select>
                            <label>Importancia del evento</label>
                        </div>
                    </div>
                    -->
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="Tipo" id="TipoEventoA" required>
                                <option value="1">Frontend</option>
                                <option value="2">Backend</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Fotografías</span>
                                <input type="file" name="userfile[]" id="userfile" multiple required>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l12 fotos">
                        
                    </div>
                    <button class="btn red darken-2 waves-effect waves-light right" id="editarevento">Registrar</button>
                </form>
            </div>
        </div>
        <div id="s3" class="row cont">
            <div class="col s12 m12 l12 contenido">
                <label class="titulo">Mensajes</label>
                <button class="col s12 m12 l2 btn red teal accent-4 right" type="button" data-action="ss3">Nuevo</button>
                <table class="responsive-table tablas white">
                    <thead>
                        <tr>
                            <th>Remitente</th>
                            <th>Mensaje</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($revista)){
                        foreach($revista as $key =>$value){
                        echo '<tr>
                            <td data-id="revista-'.$value["ID"].'" value="'.$revista[$key]["ID"].'">'.$value["Nombre"].'</td>
                            <td data-id="revista-'.$value["ID"].'" value="'.$revista[$key]["ID"].'">'.$value["Url"].'</td>
                            <td data-id="revista-'.$value["ID"].'" value="'.$revista[$key]["ID"].'"><a class="btn-flat eliminarrevista" data-id="'.$value["ID"].'"><i class="fa fa-trash"></i></a></td>
                            <td data-id="revist-'.$value["ID"].'" value="'.$revista[$key]["ID"].'"><a class="btn-flat datoseditarrevista" data-id="'.$value["ID"].'" data-action="sss3"><i class="fa fa-pencil"></i></a></td>
                        </tr>';
                        }
                        }else{
                        echo null;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="ss3" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Nuevo mensaje</label>
                <form class="white" style="margin:20px 0 20px 0;" method="post" action="<?= base_url("admin/revista")?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="NombreRevista" name="NombreRevista" class="validate">
                            <label for="NombreRevista">Destinatario</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="URLRevista" name="URLRevista" class="validate">
                            <label for="URLRevista">Mensaje</label>
                        </div>
                    </div>
                    <div id="nuevarevista" class="btn red darken-2 waves-effect waves-light right">Registrar</div>
                </form>
            </div>
        </div>
        <div id="sss3" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Actualizar Mensaje</label>
                <form class="white" style="margin:20px 0 20px 0;" method="post" action="" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="id-r" value="" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="NombreRevista" id="NombreRevistaA" class="validate">
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="URLRevista" id="URLRevistaA" class="validate">
                            
                        </div>
                    </div>
                    <button class="btn red darken-2 waves-effect waves-light right" type="submit" id="editarrevista">Actualizar</button>
                </form>
            </div>
        </div>
        <div id="s4" class="cont">
            <div class="col s12 m12 l12 contenido">
                <label class="titulo">Proyectos</label>
                <button class="col s12 m12 l2 btn red teal accent-4 right" type="button" data-action="ss4">Nuevo</button>
                <table class="responsive-table tablas white">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Titulo</th>
                            <th>Descripción</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($secciones) && !empty($secciones)){
                        foreach($secciones as $key =>$value){
                        echo '
                        <tr>
                            <td data-id="secciones-'.$value["ID"].'">'.$value["ID"].'</td>
                            <td data-id="secciones-'.$value["ID"].'">'.$value["Titulo"].'</td>
                            <td data-id="secciones-'.$value["ID"].'">'.$value["Subtitulo"].'</td>
                            <td data-id="secciones-'.$value["ID"].'">'.$value["Fecha"].'</td>
                            <td data-id="secciones-'.$value["ID"].'">'.$value["Visible"].'</td>
                            <td data-id="secciones-'.$value["ID"].'" value="'.$secciones[$key]["ID"].'"><a class="btn-flat eliminarseccion" data-id="'.$value["ID"].'"><i class="fa fa-trash"></i></a></td>
                            <td data-id="secciones-'.$value["ID"].'" value="'.$secciones[$key]["ID"].'"><a class="btn-flat datosfotosseccion datoseditarseccion" data-id="'.$value["ID"].'" data-action="sss4" ><i class="fa fa-pencil"></i></a></td>
                        </tr>';
                        }
                        }else{
                        echo NULL;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="ss4" class="row cont">
            <label class="titulo">Crear Proyecto</label>
            <div class="col s12 contenido">
                <form class="col s12 white" style="margin:20px 0 20px 0;" method="post" action="<?php echo base_url("admin/seccion/nuevo");?>" enctype="multipart/form-data">
                    <script src="../js/CKEditor/ckeditor.js"></script>
                    <div class="row" style="display: none">
                        <div class="input-field col s6">
                            <select name="TipoSeccion" id="seccion">
                                <option value="1" selected="selected">Look</option>
                                <option value="2">Viajero</option>
                                <option value="3">Con gusto</option>
                                <option value="4">La sesion</option>
                                <option value="5">Quien soy</option>
                                <option value="6">Portada sustancial</option>
                                <option value="7">Cocina de</option>
                                <option value="8">Acciones</option>
                                <option value="9">Salud</option>
                                <option value="10">Ciudadano activo</option>
                                <option value="11">Figura</option>
                                <option value="12">Cultura</option>
                            </select>
                            <label for="seccion">Tipo de seccion</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input name="Titulo" id="TituloSeccion" type="text" class="validate">
                            <label for="titulo">Titulo del proyecto</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="Subtitulo" type="text" class="validate">
                            <label for="subtitulo">Descripcion</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1 col s12">
                                <span>Agregar Fotografías</span>
                                <input name="userfile[]" type="file" multiple>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="editor" name="Contenido"></textarea>
                            <script>CKEDITOR.replace('editor');</script>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn red darken-2 waves-effect waves-light right-align" type="submit" name="action">Crear
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="sss4" class="row cont">
            <label class="titulo">Actualizar Proyecto</label>
            <div class="col s12 contenido">
                <form class="col s12 white" style="margin:20px 0 20px 0;" method="post" action="" enctype="multipart/form-data">
                    <script src="../js/CKEditor/ckeditor.js"></script>
                    <div class="row" style="display: none">
                        <div class="input-field col s12">
                            <input type="text" id="id-s" value="" disabled>
                        </div>
                    </div>
                    <div class="row" style="display: none">
                        <div class="input-field col s6">
                            <select name="seccion" id="seccionA" class="as">
                                <option value="0" disabled>Selecciona una seccion</option>
                                <option value="1">Look</option>
                                <option value="2">Viajero</option>
                                <option value="3">Con gusto</option>
                                <option value="4">La sesion</option>
                                <option value="5">Quien soy</option>
                                <option value="6">Portada sustancial</option>
                                <option value="7">Cocina de</option>
                                <option value="8">Acciones</option>
                                <option value="9">Salud</option>
                                <option value="10">Ciudadano activo</option>
                                <option value="11">Figura</option>
                                <option value="12">Cultura</option>
                            </select>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input name="titulo" id ="TituloSeccionA"type="text" class="validate">
                            
                        </div>
                        <div class="input-field col s6">
                            <input name="subtitulo" id="SubtituloSeccionA" type="text" class="validate">
                            
                            <!--
                            <p>
                                <input type="checkbox" id="test" name="author">
                                <label for="test">Yo soy el autor</label>
                            </p>
                            -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1 col s12">
                                <span>Agregar Fotografías</span>
                                <input name="userfile[]" type="file" multiple>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="editorA" name="editorA"></textarea>
                            <script>CKEDITOR.replace('editorA');</script>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn red darken-2 waves-effect waves-light right-align" type="submit" id="editarseccion">actualizar
                            </button>
                        </div>
                    </div>
                    <div class="fotosSec">
                    </div>
                </form>
            </div>
        </div>
        <div id="s5" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Estadisticas</label>
                <table class="responsive-table striped white">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Fecha </th>
                            <th>Titulo</th>
                            <th>Subtitulo</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($columnas) && !empty($columnas)){
                        foreach($columnas as $key =>$value){
                        echo '
                        <tr id="evento-'.$value["ID"].'">
                            <td data-id="columna-'.$value["ID"].'">'.$value["ID"].'</td>
                            <td data-id="columna-'.$value["ID"].'">'.$value["Fecha"].'</td>
                            <td data-id="columna-'.$value["ID"].'">'.$value["Titulo"].'</td>
                            <td data-id="columna-'.$value["ID"].'">'.$value["Subtitulo"].'</td>
                            <td data-id="columna-'.$value["ID"].'" value="'.$columnas[$key]["ID"].'"><a class="btn-flat eliminarcolumna" data-id="'.$value["ID"].'"><i class="fa fa-trash"></i></a></td>
                            <td data-id="columna-'.$value["ID"].'" value="'.$columnas[$key]["ID"].'"><a class="btn-flat datoseditarcolumna" data-id="'.$value["ID"].'" data-action="sss5"><i class="fa fa-pencil"></i></a></td>
                        </tr>';
                        }
                        }else{
                        echo null;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div id="s6" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Configuraciones</label>
                <div class="row">
                    <?php
                    if(isset($eventos) && !empty($eventos)){
                    $leer = 0;
                    $ruta = "img/eventos/".$value["ID"]."/"; // Indicar ruta
                    $filehandle = opendir($ruta); // Abrir archivos
                    while($leer == 0){
                    $file = readdir($filehandle);
                    if ($file != "." && $file != "..") {
                    $tamanyo = GetImageSize($ruta . $file);
                    $leer =1;
                    }
                    }
                    closedir($filehandle);
                    
                    foreach($eventos as $key =>$value){
                    echo '<div class="col s4 m4 taggeo" data-id="'.$value["ID"].'" data-action="ss6">
                        <div class="card">
                            <div class="card-image">
                                <img src="'.base_url("img/eventos/".$value["ID"]."/".$file).'">
                                <span class="card-title">'.$value["Nombre"].'</span>
                            </div>
                        </div>
                    </div>';
                    }
                    }else{
                    echo null;
                    }
                    ?>
                    
                </div>
            </div>
        </div>
        <div id="ss6" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Taggeo de Fotografias</label>
                <button class="col s12 m12 l2 btn red teal accent-4 right" type="button" data-action="sss6">Nuevo</button>
                <input type="text" id="ev-tag" hidden>
                <table class="responsive-table striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre </th>
                            <th>Fecha</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody class="tag">
                    </tbody>
                </table>
            </div>
        </div>
        <div id="sss6" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Nuevo Taggeo</label>
                <form style="margin:20px 0 20px 0;" enctype="multipart/form-data">
                    <script src="../js/CKEditor/ckeditor.js"></script>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="Nombre-tag" id="Nombre-tag"class="validate">
                            <label for="Nombre-tag">Nombre</label>
                        </div>
                    </div>
                    <button class="btn red darken-2 waves-effect waves-light right" id="nuevotag">Registrar</button>
                </form>
            </div>
        </div>
        <div id="ssss6" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Actualizar Taggeo</label>
                <form style="margin:20px 0 20px 0;" enctype="multipart/form-data">
                    <script src="../js/CKEditor/ckeditor.js"></script>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="Nombre-tag-e" id="Nombre-tag-e"class="validate">
                            
                        </div>
                    </div>
                    <button class="btn red darken-2 waves-effect waves-light right" id="nuevotag">Registrar</button>
                </form>
            </div>
        </div>
        <div id="s8" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Usuarios</label>
                <button class="col s12 m12 l2 btn red teal accent-4 right" type="button" data-action="ss8"> Nuevo </button>
                <table class="responsive-table striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th>Password</th>
                            <th>Fecha de Registro</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($usuarios) && !empty($usuarios)){
                        $num =  count($usuarios);
                        foreach($usuarios as $key =>$value){
                        echo '
                        <tr id="usuario-'.$value["ID"].'">
                            <td data-id="columna-'.$value["ID"].'">'.$value["Nombre"].'</td>
                            <td data-id="columna-'.$value["ID"].'">'.$value["Correo"].'</td>
                            <td data-id="columna-'.$value["ID"].'"> *********** </td>
                            <td data-id="columna-'.$value["ID"].'">'.$value["Fecha"].'</td>
                            <td data-id="columna-'.$value["ID"].'" value="'.$usuarios[$key]["ID"].'">';
                                if($num>1){
                                echo '<a class="btn-flat eliminarusuario" data-id="'.$value["ID"].'"><i class="material-icons">delete</i></a>';
                                }
                                echo
                            '</td>
                            <td data-id="columna-'.$value["ID"].'" value="'.$usuarios[$key]["ID"].'"><a class="btn-flat getusuario" data-id="'.$value["ID"].'" data-action="sss8"><i class="material-icons">mode_edit</i></a></td>
                        </tr>';
                        }
                        }else{
                        echo null;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="ss8" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Nuevo Usuario</label>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="nombreUsuario" type="text" class="validate">
                        <label for="nombreUsuario">Nombre Completo</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="correoUsuario" type="text" class="validate">
                        <label for="correoUsuario">Nombre de Usuario</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="passwordUsuario" type="password" class="validate">
                        <label for="passwordUsuario">Password</label>
                    </div>
                </div>
                <button id="nuevousuario" class="btn red darken-2 waves-effect waves-light right">Registrar</button>
            </div>
        </div>
        <div id="sss8" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Editar Usuario</label>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="EnombreUsuario" type="text" class="validate">
                        
                    </div>
                    <div class="input-field col s12">
                        <input id="EcorreoUsuario" type="text" class="validate">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="EpasswordUsuario" type="password" class="validate">
                        
                    </div>
                </div>
                <button id="editarusuario" class="btn red darken-2 waves-effect waves-light right">Registrar</button>
            </div>
        </div>
        <div id="s7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Banners</label>
                <div class="row">
                    <div class="col s4 m4 banner-seccion" data-id="1" data-action="ss7">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?= base_url("img/banners/thumbnails/inicio.png") ?>">
                                <span class="card-title">Inicio</span>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 m4 banner-seccion" data-id="2" data-action="sss7">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?= base_url("img/banners/thumbnails/eventos.png")?>">
                                <span class="card-title">Eventos</span>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 m4 banner-seccion" data-id="3" data-action="ssss7">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?= base_url("img/banners/thumbnails/evento.png")?>">
                                <span class="card-title ">Evento</span>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 m4 banner-seccion" data-id="4" data-action="sssss7">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?= base_url("img/banners/thumbnails/secciones.png") ?>">
                                <span class="card-title">Secciones</span>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 m4 banner-seccion" data-id="5" data-action="ssssss7">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?= base_url("img/banners/thumbnails/contacto.png")?>">
                                <span class="card-title">Contacto</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="ss7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Banners Inicio</label>
                <div class="row">
                    <input disabled type="text" id="seccion-inicio" value="">
                    <div class="col s8" data-action="sssssss7" data-id="1">
                        <img style="width: 600px" src="<?= base_url("img/maquetado/Index.jpg")?>">
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="1">Primero
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="2">Segundo
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="3">Tercero
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="4">Cuarto
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="5">Quinto
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="6">Sexto
                    </div>
                </div>
            </div>
        </div>
        <div id="sss7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Banners eventos</label>
                <div class="row">
                    <input disabled type="text" id="seccion-eventos" value="">
                    <div class="col s8" data-action="sssssss7" data-id="1">
                        <img style="width: 600px" src="<?= base_url("img/maquetado/Eventos.jpg")?>">
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light  banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="1">Primero
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light  banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="2">Segundo
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light  banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="3">Tercero
                    </div>
                </div>
            </div>
        </div>
        <div id="ssss7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Banners evento</label>
                <div class="row">
                    <input disabled type="text" id="seccion-evento" value="">
                    <div class="col s8" data-action="sssssss7" data-id="1">
                        <img style="width: 600px" src="<?= base_url("img/maquetado/Evento.jpg")?>">
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light  banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="1">Primero
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light  banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="2">Segundo
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light  banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="3">Tercero
                    </div>
                </div>
            </div>
        </div>
        <div id="sssss7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Banners Secciones</label>
                <div class="row">
                    <input disabled type="text" id="seccion-secciones" value="">
                    <div class="col s8" data-action="sssssss7" data-id="1">
                        <img style="width: 600px" src="<?= base_url("img/maquetado/Secciones.jpg")?>">
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="1">Primero
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="2">Segundo
                    </div>
                </div>
            </div>
        </div>
        <div id="ssssss7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Banners contacto</label>
                <div class="row">
                    <input disabled type="text" id="seccion-contacto" value="">
                    <div class="col s8" data-action="sssssss7" data-id="1">
                        <img style="width: 600px" src="<?= base_url("img/maquetado/Contacto.jpg")?>">
                    </div>
                    <div class="col s4 btn red darken-2 waves-effect waves-light banner-datos" style="margin-bottom: 5px;" data-action="sssssss7" data-id="1">Primero
                    </div>
                </div>
            </div>
        </div>
        <div id="sssssss7" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Cargar Banner</label>
                <form class="col s12" style="margin:20px 0 20px 0;" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="posicion-ban" id="posicion-ban" class="validate" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="Cliente" id="Cliente" class="validate" required>   
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="Enlace" id="Enlace" class="validate" required>   
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Banner<as></as></span>
                                <input type="file" name="userfile[]" id="banner" multiple required>
                            </div>
                        </div>
                    </div>
                    <div class="btn red darken-2 waves-effect waves-light right nuevobanner">Registrar</div>
                    <div class="col s12 m12 l12 fotosb"></div>
                </form>
            </div>
        </div>

        <div id="s9" class="row cont">
            <div class="col s12 m12 l12 contenido">
                <label class="titulo">Portadas</label>
                <button class="col s12 m12 l2 btn red teal accent-4 right" type="button" data-action="ss9">Nuevo</button>
                <table class="responsive-table striped tablas">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($portadas) && !empty($portadas)){
                        foreach($portadas as $key =>$value){
                        echo '<tr>
                            <td data-id="portada-'.$value["ID"].'" value="'.$portadas[$key]["ID"].'">'.$value["ID"].'</td>
                            <td data-id="portada-'.$value["ID"].'" value="'.$portadas[$key]["ID"].'">'.$value["Nombre"].'</td>
                            <td data-id="portada-'.$value["ID"].'" value="'.$portadas[$key]["ID"].'">'.$value["Fecha"].'</td>
                            <td data-id="portada-'.$value["ID"].'" value="'.$portadas[$key]["ID"].'"><a class="btn-flat eliminarportada" data-id="'.$value["ID"].'"><i class="material-icons">delete</i></a></td>
                            <td data-id="portada-'.$value["ID"].'" value="'.$portadas[$key]["ID"].'"><a class="btn-flat datoseditarportada" data-id="'.$value["ID"].'" data-action="sss9"><i class="material-icons">mode_edit</i></a></td>
                        </tr>';
                        }
                        }else{
                        echo null;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="ss9" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Nueva Portada</label>
                <form style="margin:20px 0 20px 0;" method="post" id="portada" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="NombrePortada" name="NombrePortada" class="validate">
                            <label for="NombrePortada">Nombre</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="date" name="FechaPortada" id="FechaPortada" class="datepicker" required>
                            <label for="FechaPortada">Fecha</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="ImagenPortada1" name="ImagenPortada1" class="validate" disabled>
                            <label for="FechaPortada"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Imagen<as></as></span>
                                <input type="file" name="file" id="file" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="ImagenPortada2" name="ImagenPortada2" class="validate" disabled>
                            <label for="FechaPortada"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Imagen<as></as></span>
                                <input type="file" name="file2" id="file2" required>
                            </div>
                        </div>
                    </div>
                    <div id="nuevaportada" class="btn red darken-2 waves-effect waves-light right">Registrar</div>
                </form>
            </div>
        </div>
        <div id="sss9" class="row cont">
            <div class="col s12 m12 l12">
                <label class="titulo">Actualizar Portada</label>
                <form style="margin:20px 0 20px 0;" method="post" id="portada" enctype="multipart/form-data">
                <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="id-pa" value="" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="NombrePortadaA" name="NombrePortadaA" class="validate">
                        </div>
                    </div>
                   <div class="row">
                        <div class="input-field col s12">
                            <input type="date" name="FechaPortadaA" id="FechaPortadaA" class="datepicker" required>
                        </div>
                    </div>
                    <div class="FotoPortada1"></div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="ImagenPortada1A" name="ImagenPortada1A" class="validate" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Imagen<as></as></span>
                                <input type="file" name="fileA" id="fileA" required>
                            </div>
                        </div>
                    </div>
                    <div class="FotoPortada2"></div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="ImagenPortada2A" name="ImagenPortada2A" class="validate" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn blue-grey darken-1">
                                <span>Agregar Imagen<as></as></span>
                                <input type="file" name="file2A" id="file2A" required>
                            </div>
                        </div>
                    </div>
                    <div id="actualizarportada" class="btn red darken-2 waves-effect waves-light right">Registrar</div>
                </form>
            </div>
        </div>
        <div id="DivConfiguraciones" class="row cont">
            <div class="col s10 push-s1">
                
                <ul class="collapsible popout" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header">Secciones</div>
                        <div class="collapsible-body">
                            <div class="row">
                                <div class="col s12">
                                    <ul class="collection">
                                        <?php
                                        if(isset($nombreSecciones) && !empty($nombreSecciones)){
                                        foreach($nombreSecciones as $key =>$value){
                                        echo "<li class='collection-item'>
                                            <div class='row' style='margin-bottom:0px'>
                                                <div class='col s8' data-texto='NombreSecciones".$key."' >
                                                    ".$value["Nombre"]."
                                                </div>
                                                <div class='col s4'>
                                                    <button class='btn' data-modificar='NombreSecciones".$key."'><i class='fa fa-pencil'></i></button>
                                                    <button class='btn'><i class='fa fa-trash'></i></button>
                                                </div>
                                            </div>
                                        </li>";
                                        }
                                        }
                                        ?>
                                        <li class='collection-item'>
                                            <div class="row">
                                                <div class="col s1">Nuevo:</div>
                                                <div class="col s9">
                                                    <input type="text" placeholder="Nuevo">
                                                </div>
                                                <button class="col s2 btn">Nuevo</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header">Directorio</div>
                        <div class="collapsible-body">
                            <div class="row">
                                <div class="col s10 push-s1">
                                    <table class="centered responsive-table">
                                        <thead>
                                            <tr>
                                                <th data-field="id">Cargo</th>
                                                <th data-field="name">Nombre</th>
                                                <th data-field="name">Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Alvin</td>
                                                <td>Eclair</td>
                                                <td>
                                                    <button class='btn'><i class='fa fa-pencil'></i></button>
                                                    <button class='btn'><i class='fa fa-trash'></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header">Privacidad</div>
                        <div class="collapsible-body">
                            <script src="../js/CKEditor/ckeditor.js"></script>  
                            <textarea id="editor3" name="editor3"></textarea>
                            <script>CKEDITOR.replace('editor3');</script>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header">Datos</div>
                        <div class="collapsible-body">
                            
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- FIN CONTENIDO-->
    </div>
</div>