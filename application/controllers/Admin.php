<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('madmin');
        $this->load->library(array('session','form_validation','encrypt','email',"libreria"));
        $this->load->helper('url');
        $this->load->helper('security');
	}

//Inicio metodo index
	public function index()
	{
		
	   if($this->session->userdata('identificador')){
            redirect("admin/dashboard","refresh");
        }else{
            redirect("admin/login","refresh");
        }
	
	}
//Fin metodo index
//Inicio metodo dashboard
	public function dashboard()
	{
		//Inicio vista dashboard
		//Inicio Codigo dashboard
		//Fin Codigo dashboard
		$array = array(
                 'identificador' => 1);
        $this->session->set_userdata($array);
		if($this->session->userdata('identificador')){
            $data_head["titulo"]="Panel Administrador | City En Linea";
            $data_head["descripcion"]="Revista Soocial";
            $data_head["imagen"]="";
            $data_head["css"] = array("materialize.min","font-awesome.min");
            $data_head["keywords"]="Revista, Social, San Luis Potosí";
            $data_head["ico"]=base_url()."img/ico.png";
           

            $data_javascript["script"] = array("jquery","materialize.min","dropdown","datatable","admin/dashboard","materializeinit");

            // OBTIENE SECCION DE VIDEOS
            $data_contenido["videos"] = ($this->madmin->getvideos()!=FALSE)? $this->madmin->getvideos(): NULL;

            // OBTIENE SECCION DE EVENTOS
            $data_contenido["eventos"] = ($this->madmin->geteventos()!=FALSE)? $this->madmin->geteventos() : NULL;

            // OBTIENE SECCION DE COLUMNAS
            $data_contenido["columnas"] = ($this->madmin->getcolumnas()!=FALSE)? $this->madmin->getcolumnas(): NULL;

            // OBTIENE SECCION DE BLOG
            $blog=$this->madmin->getsecciones();
            if($blog!=false){
                    foreach($blog as $seccion =>$valor){
                        if($valor['Habilitado']==1){
                            $blog[$seccion]["Visible"]="En Linea";
                        }else{
                            $blog[$seccion]["Visible"]="No Mostrado";
                        }
                        $blog[$seccion]["TipoSeccion"] = $this->libreria->get_seccion($valor['TipoSeccion']);

                    }
                $data_contenido["secciones"]=$blog;
            ////////////////////////////////////////
            }else{$data_contenido["secciones"]=NULL;}
            ////////////////////////////////////////
            
            //OBTIENE SECCION DE EDICIONES
            $data_contenido["revista"] = ($this->madmin->getrevistas()!=FALSE)? $this->madmin->getrevistas(): NULL;
            // OBTIENE SECCION DE PORTADAS
            $data_contenido["portadas"] = ($this->madmin->getportadas()!=FALSE)? $this->madmin->getportadas(): NULL;

             // OBTIENE SECCION DE BANNERS
            $data_contenido["banners"] = ($this->madmin->getbanners()!=FALSE)? $this->madmin->getbanners(): NULL;

            $data_contenido["usuarios"] = ($this->madmin->getusuarios()!=FALSE)? $this->madmin->getusuarios(): NULL;

            $data_contenido["nombreSecciones"] = ($this->madmin->getNombresSecciones()!=FALSE)? $this->madmin->getNombresSecciones(): NULL;

            // CARGA CONTENIDO A VISTAS
            $data["head"] = $this->load->view("head",$data_head,TRUE);
            $data["javascript"] = $this->load->view("javascript",$data_javascript,TRUE);
            $data["contenido"] = $this->load->view("admin/dashboard",$data_contenido,TRUE);
            $this->load->view("html",$data);

        }else{
            redirect("admin/login","refresh");
        }
		//Fin vista dashboard
		
	}
//Fin metodo dashboard
//Inicio metodo login
	public function login()
	{
        if($this->session->userdata('identificador')){
            redirect('admin/dashboard','refresh');
        }else{
    		if($this->input->is_ajax_request()){
    			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
    			if($this->input->post()==NULL){
    				//Inicio AJAX/GET login
    				$array = array("Ajax" => TRUE, "METHOD" => "GET");
    				$this->output->set_content_type("application/json")->set_output(json_encode($array));
    				//Fin AJAX/GET login
    			}else{
    				//Inicio AJAX/POST login
    				$array = array("Ajax" => TRUE, "METHOD" => "POST");
    				$this->output->set_content_type("application/json")->set_output(json_encode($array));
    				//Fin AJAX/POST login
    			}
    		}elseif($this->input->is_ajax_request()==FALSE && ($this->input->post()!=NULL||$this->input->get()!=NULL)){
    			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
    			if($this->input->post()==NULL){
    				//Inicio NOAJAX/GET login
    				$array = array("Ajax" => FALSE, "METHOD" => "GET");
    				$this->output->set_content_type("application/json")->set_output(json_encode($array));
    				//Fin NOAJAX/GET login
    			}else{
    				//Inicio NOAJAX/POST login
    				$this->form_validation->set_rules('Correo', 'Correo electrónico', 'trim|required');
                    $this->form_validation->set_rules('Password', 'Contraseña', 'trim|required');
                    if ($this->form_validation->run() == FALSE) {
                        redirect("admin/login/1","refresh");
                    } else {
                    	$data["Password"] = sha1(md5($data["Password"]));
                        $result = $this->madmin->login($data);
                        if($result!=FALSE){
                            $result = $result["0"]["ID"];
                            $array = array(
                                'identificador' => $result,
                            );
                            $this->session->set_userdata($array);
                            redirect('admin/dashboard','refresh');
                        }else{
                            redirect('admin/login/2','refresh');
                        }
                    }
                    //Fin NOAJAX/POST login
    			}
    		}else{
    			//Inicio vista login
    			//Inicio Codigo login
    			//Fin Codigo login
    			$data_head["titulo"]="Ingresar | City En Linea";
                $data_head["descripcion"]="Revista Soocial";
                $data_head["imagen"]="";
                $data_head["keywords"]="Revista, Social, San Luis Potosí";
                $data_head["ico"]=base_url()."img/ico.png";
                $data_head["css"] = array("materialize.min");
                $data_javascript["script"] = array("jquery","materialize.min");
				
				$data_contenido["error"] = ($this->uri->segment(3)!=NULL)? $this->uri->segment(3):0;
                $data["head"] = $this->load->view("head",$data_head,TRUE);
                $data["javascript"] = $this->load->view("javascript",$data_javascript,TRUE);
                $data["contenido"] = $this->load->view("admin/login",$data_contenido,TRUE);
                $this->load->view("html",$data);
    			//Fin vista login
    		}
        }
	}
//Fin metodo login
//Inicio metodo logout
	public function logout()
	{
		$this->session->unset_userdata('identificador');
        redirect('admin/login','refresh');
	}
//Fin metodo logout
//Inicio metodo geteventos
	public function geteventos()
	{
		if($this->input->is_ajax_request()){
			if($this->input->post()==NULL){
				//Inicio AJAX/GET geteventos
				$array = $this->madmin->geteventos();
				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/GET geteventos
			}
		}
	}
//Fin metodo geteventos
//Inicio metodo getevento (admin/evento)
	public function getevento()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
                if($this->input->post()==NULL){
                //Inicio AJAX/GET getevento
                    if(is_numeric($id)&&$id>0){
                        $array = $this->madmin->getevento($id);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }
                //Fin AJAX/GET getevento
            }
		}
	}
//Fin metodo getevento
    public function getfotosevento(){
        if($this->input->is_ajax_request()){
            if($this->uri->segment(4) == "fotos"){
                $id = $this->uri->segment(3);
                if($this->input->post()==NULL){
                    //Inicio AJAX/GET getevento
                    if(is_numeric($id)&&$id>0){
                        $array = $this->madmin->geteventofotos($id);
                        $array = $array["0"];
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }
                    //Fin AJAX/GET getevento
                }
            }
        }
    }
    public function eliminarfoto()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->get()==NULL){
                //Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->eliminarfotoevento($id);
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));   
                }
                //Fin AJAX/POST eliminarevento
            }
        }
    }
//Inicio metodo editarevento
	public function editarevento()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->post()!=NULL){
                //Inicio AJAX/POST nuevoevento
                foreach ($data as $key => $value) {
                    $data["$key"] = $this->security->xss_clean($value);
                }
                $data['IDUsuario']= $this->session->userdata('identificador');
                $this->form_validation->set_rules('Evento','Titulo del Evento','trim|required|max_length[30]');
                $this->form_validation->set_rules('Fecha','Fecha del Evento','trim|required|min_length[10]');
                $this->form_validation->set_rules('Lugar','Lugar del Evento','trim|required|max_length[130]');
                $this->form_validation->set_rules('Tipo','Tipo del Evento','trim|required|integer');
                $s=explode(' ',$data['Fecha']);
                $w=explode(',',$s[1]);
                $mes =array(
                    'January'=>'01',
                    'February'=>'02',
                    'March'=>'03',
                    'April'=>'04',
                    'May'=>'05',
                    'June'=>'06',
                    'July'=>'07',
                    'August'=>'08',
                    'September'=>'09',
                    'October'=>'10',
                    'November'=>'11',
                    'December'=>'12'
                );
                foreach($mes as $month => $number){
                    if($w[0]==$month){
                        $value=$number;
                    }
                }
                $data['Fecha'] = $s[2]."-".$value."-".$s[0];
                //$id = $this->madmin->insert_evento($datos);
                $this->madmin->editarevento($id,$data);
                $band = TRUE; //La bandera se inicializa en TRUE
                $idFotoError = 0; 

                $this->load->library('upload');
                $this->load->library('image_lib');
                
                $files=$_FILES;
                $cpt=count($_FILES['userfile']['name']);
                for($i=0;$i<$cpt;$i++){
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                    $link = "./img/eventos/".$id;

                    $this->upload->initialize($this->libreria->set_upload_options($link));
                    
                    if($this->upload->do_upload()==FALSE){
                        $band=FALSE;
                    }else{
                        $data = array("IDEvento" => $id,
                            "Nombre" => $_FILES['userfile']['name'],
                            "Habilitado" => 1);
                        $this->madmin->insertarimagenevento($data);
                        $this->setWatermark($link, $_FILES['userfile']['name']);
                    }
                }
                if($band!=FALSE){
                    //Se subieron todas la imagenes correctamente
                    redirect("admin/dashboard","refresh");
                }else{
                    //Hubo un error al subir imagenes
                    redirect("admin/dashboard","refresh");
                }
            //Fin AJAX/POST nuevoevento
            }
		}
	}
//Fin metodo editarevento
//Inicio metodo eliminarevento
	public function eliminarevento()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$return = $this->madmin->eliminarevento($id);
	                $array  = array("result" => $return);
					$this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
//Fin metodo eliminarevento
//Inicio metodo nuevoevento
	public function nuevoevento()
	{
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevoevento
				foreach ($data as $key => $value) {
					$data["$key"] = $this->security->xss_clean($value);
				}
				$data["Nombre"] = $data["Evento"];
				
                unset($data["Evento"]);
                $data['IDUsuario']= $this->session->userdata('identificador');
				$this->form_validation->set_rules('Evento','Titulo del Evento','trim|required|max_length[30]');
                $this->form_validation->set_rules('Fecha','Fecha del Evento','trim|required|min_length[10]');
                $this->form_validation->set_rules('Lugar','Lugar del Evento','trim|required|max_length[130]');
                $this->form_validation->set_rules('Tipo','Tipo del Evento','trim|required|integer');
				$s=explode(' ',$data['Fecha']);
				$w=explode(',',$s[1]);
                $mes =array(
                    'January'=>'01',
                    'February'=>'02',
                    'March'=>'03',
                    'April'=>'04',
                    'May'=>'05',
                    'June'=>'06',
                    'July'=>'07',
                    'August'=>'08',
                    'September'=>'09',
                    'October'=>'10',
                    'November'=>'11',
                    'December'=>'12'
                );
                foreach($mes as $month => $number){
                	if($w[0]==$month){
                		$value=$number;
                	}
                }
                $data['Fecha'] = $s[2]."-".$value."-".$s[0];
                //$id = $this->madmin->insert_evento($datos);
                $id = $this->madmin->nuevoevento($data);
                $band = TRUE; //La bandera se inicializa en TRUE
                $idFotoError = 0; 
                if($id!=false){
                    $this->madmin->asignarImportanciaInicial($id);

                    $this->load->library('image_lib');
                    $this->load->library('upload');
                    $files=$_FILES;
                    $cpt=count($_FILES['userfile']['name']);

                    if(!is_dir("../img/eventos/".$id."/")){
                       mkdir("../img/eventos/".$id."/",0777);
                    }
                    $this->load->library('image_lib');


                    for($i=0;$i<$cpt;$i++){
                        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                        $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                        $link = "./img/eventos/".$id;

                        $this->upload->initialize($this->libreria->set_upload_options($link));
                        
                    	if($this->upload->do_upload()==FALSE){
                        	$band=FALSE;
                            redirect("admin/dashboard?error=2&tipo=eventos","refresh");
                    	}else{
                    		$data = array("IDEvento" => $id,
                    			"Nombre" => $_FILES['userfile']['name'],
                    			"Habilitado" => 1);
                    		$this->madmin->insertarimagenevento($data);
                            $data_img = $this->upload->data();
                    		$this->setWatermark($link, $data_img["file_name"]);
                            redirect("admin/dashboard?correcto=1&tipo=eventos","refresh");
                    	}
                    }
                }else{
                	//Hubo error al insertar el evento
                    redirect("admin/dashboard?error=1&tipo=eventos","refresh");
                }
				//Fin AJAX/POST nuevoevento
			}
	}

	public function setWatermark($path, $name){

		if(!is_dir($path . "/watermark/")){
            mkdir($path . "/watermark/",0777);
        }
		
		$config['image_library']    = 'gd2';
        $config['source_image']     =  $path . "/" . $name;
        $config['wm_type']          = 'overlay';
        $config['wm_overlay_path']  = './img/logo-black2.png';
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_opacity']       = 50;
        $config['width']         	= 50;
		$config['height']       	= 30;
        $config['new_image'] 		= $path . "/watermark/" . $name;
        
        $this->image_lib->initialize($config);
        
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
	}
//Fin metodo nuevoevento
//Inicio metodo getvideos
	public function getvideos()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getvideos
				$array = $this->madmin->getvideos();
				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/GET getvideos
			}
		}
	}
//Fin metodo getvideos
//Inicio metodo getvideo
	public function getvideo()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getevento
				if(is_numeric($id)&&$id>0){
					$array = $this->madmin->getvideo($id);
					$array = $array["0"];
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/GET getevento
			}
		}
	}
//Fin metodo getvideo
//Inicio metodo editarvideo
	public function editarvideo()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST editarvideo
				if(is_numeric($id)&&$id>0){
					foreach ($data as $key => $value) {
						$data["$key"] = $this->security->xss_clean($value);
					}
                    $posicion = strpos($data["Url"],"v=");
                    $data["Url"] = substr($data["Url"],$posicion+2,12);
                    $array = array("result" => $this->madmin->editarvideo($id,$data));
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/POST editarvideo
			}
		}
	}
//Fin metodo editarvideo
//Inicio metodo eliminarvideo
	public function eliminarvideo()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$return = $this->madmin->eliminarvideo($id);
	                $array  = array("result" => $return);
					$this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
//Fin metodo eliminarvideo
//Inicio metodo nuevovideo
	public function nuevovideo()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				foreach ($data as $key => $value) {
					$data["$key"] = $this->security->xss_clean($value);
				}
				$posicion = strpos($data["Url"],"v=");
		        $data["Url"] = substr($data["Url"],$posicion+2);
		        $this->form_validation->set_rules('Nombre','Nombre del Video','trim|required|max_length[130]');
		        $this->form_validation->set_rules('URL','URL de Youtube','trim|required|max_length[64]');
		        if ($this->form_validation->run() == FALSE) {
		        	if($this->madmin->nuevovideo($data)){
		        		$array = array("result" => TRUE,"posicion"=>$posicion);
		        	}else{
		        		$array = array("result" => FALSE,"descripcion"=> "Falla en el modelo");
		        	}
		        } else {
		        	$array = array("result" => FALSE,"descripcion" => "Falla de datos");
		        }

				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/POST nuevovideo
			}
		}
	}
//Fin metodo nuevovideo
//Inicio metodo getsecciones
	public function getsecciones()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getvideos
				$array = $this->madmin->getsecciones();
				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/GET getvideos
			}
		}
	}
//Fin metodo getsecciones
//Inicio metodo getseccion
	public function getseccion()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getevento
				if(is_numeric($id)&&$id>0){
					$array = $this->madmin->getseccion($id);
					$array = $array["0"];
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/GET getevento
			}
		}
	}
    public function getfotosseccion(){
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getevento
                if(is_numeric($id)&&$id>0){
                    if($this->uri->segment(4)=="fotos"){
                        $array = $this->madmin->getfotosseccion($id);
                        $array = $array["0"];
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }
                }
                //Fin AJAX/GET getevento
            }
        }
    }
//Fin metodo getseccion
//Inicio metodo editarseccion
	public function editarseccion()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				if(is_numeric($id)&&$id>0){
					foreach ($data as $key => $value) {
						$data["$key"] = $this->security->xss_clean($value);
					}
					$array = array("result" => $this->madmin->editarseccion($id,$data));
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/POST nuevovideo
			}
		}
	}
//Fin metodo editarseccion
//Inicio metodo eliminarseccion
	public function eliminarseccion()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$return = $this->madmin->eliminarseccion($id);
	                $array  = array("result" => $return);
					$this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
//Fin metodo eliminarseccion
//Inicio metodo nuevaseccion
	public function nuevaseccion()
	{
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
                $data['IdAutor']= $this->session->userdata('identificador');
                $this->form_validation->set_rules('TipoSeccion','Tipo de seccion','trim|required|integer');
                $this->form_validation->set_rules('Titulo','Titulo de la Seccion','trim|required|max_length[100]');
                $this->form_validation->set_rules('Subtitulo','Subtitulo','trim|required|integer');
                $this->form_validation->set_rules('Contenido','Contenido','trim|required|max_length[10000]');
                unset($data["action"]);
                date_default_timezone_set("America/Mexico_City");
                $data['Fecha'] = date('Y-m-d H:i:s');
                $id = $this->madmin->nuevaseccion($data);
                $band = TRUE;
                if($id != FALSE){
                    // ---------IMAGENES----------
                    $this->load->library('upload');
                    if(!is_dir("./img/secciones/".$data["TipoSeccion"]."/".$id."/")){
                        mkdir("./img/secciones/".$data["TipoSeccion"]."/".$id."/",0777);
                    }
                    $files=$_FILES;
                    $cpt=count($_FILES['userfile']['name']);
                    $seccion = $data["TipoSeccion"];
                    for($i=0;$i<$cpt;$i++){
                        if($i == 0){
                            $_FILES['userfile']['name']= "slat-".$id.".jpg";
                        }else{
                            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                        }
                        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                        $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 
                        $link = "./img/secciones/".$seccion."/".$id."/"; 
                        $this->upload->initialize($this->libreria->set_upload_options($link));     
                        if($this->upload->do_upload()){
                            $band=true;
                            $data = array(
                                'IDseccion' => $id,
                                'Nombre' => $_FILES['userfile']['name'],
                                'Habilitado' => 1
                            );
                            if($this->madmin->insertarImagenSeccion($data)==FALSE){
                                $band=FALSE;
                            }
                        }else{
                            echo $this->upload->display_errors();
                        }
                    }
                    if($band!=false){
                        //redirect("admin/dashboard","refresh");
                    }else{
                        //redirect("admin/dashboard","refresh");
                    }
                    // ---------------------------
                }else{
                    //redirect("admin/dashboard","refresh");
                }
                //Fin AJAX/POST nuevovideo
			}
	}
//Fin metodo nuevaseccion
//Inicio metodo getrevistas
	public function getrevistas()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getvideos
				$array = $this->madmin->getrevistas();
				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/GET getvideos
			}
		}
	}
//Fin metodo getrevistas
//Inicio metodo getrevista
	public function getrevista()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getevento
				if(is_numeric($id)&&$id>0){
					$array = $this->madmin->getrevista($id);
					$array = $array["0"];
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/GET getevento
			}
		}
	}
//Fin metodo getrevista
//Inicio metodo editarrevista
	public function editarrevista()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				if(is_numeric($id)&&$id>0){
					foreach ($data as $key => $value) {
						$data["$key"] = $this->security->xss_clean($value);
					}
					$array = array("result" => $this->madmin->editarrevista($id,$data));
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/POST nuevovideo
			}
		}
	}
//Fin metodo editarrevista
//Inicio metodo eliminarrevista
	public function eliminarrevista()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$return = $this->madmin->eliminarrevista($id);
	                $array  = array("result" => $return);
					$this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
//Fin metodo eliminarrevista
//Inicio metodo nuevarevista
	public function nuevarevista()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				$data["IDUsuario"] = $this->session->userdata('identificador');
                date_default_timezone_set('America/Mexico_City');
                $data["Fecha"] = date('Y-m-d H:i:s');
                $this->form_validation->set_rules('Nombre','Titulo de la revista','trim|required|max_length[130]');
                $this->form_validation->set_rules('Url','URL de Revista','trim|required|max_length[200]');
                if($this->madmin->nuevarevista($data)!=false){
                    $array = array('result' => TRUE);
                }else{
                    $array = array('result' => FALSE);
                }

				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/POST nuevovideo
			}
		}
	}
//Fin metodo nuevarevista
//Inicio metodo getcolumnas
	public function getcolumnas()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getvideos
				$array = $this->madmin->getcolumnas();
				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/GET getvideos
			}
		}
	}
//Fin metodo getcolumnas
//Inicio metodo getcolumna
	public function getcolumna()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->post()==NULL){
				//Inicio AJAX/GET getevento
				if(is_numeric($id)&&$id>0){
					$array = $this->madmin->getcolumna($id);
					$array = $array["0"];
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/GET getevento
			}
		}
	}
//Fin metodo getcolumna
//Inicio metodo editarcolumna
	public function editarcolumna()
	{
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				if(is_numeric($id)&&$id>0){
					foreach ($data as $key => $value) {
						$data["$key"] = $this->security->xss_clean($value);
					}
					$array = array("result" => $this->madmin->editarcolumna($id,$data));
					$this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/POST nuevovideo
			}
		}
	}
//Fin metodo editarcolumna
//Inicio metodo eliminarcolumna
	public function eliminarcolumna()
	{
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$array = $this->madmin->eliminarcolumna($id);
	                $this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
//Fin metodo eliminarcolumna
//Inicio metodo nuevacolumna
	public function nuevacolumna()
	{
		//Inicio AJAX/POST nuevovideo
        $datos = $this->input->post();
        $this->form_validation->set_rules('Titulo','Nombre de Edición','trim|required|max_length[200]');
        $this->form_validation->set_rules('Subtitulo','Titulo columna','trim|required|max_length[200]');
       
        $this->form_validation->set_rules('Contenido','Contenido','trim|required|max_length[10000]');
        $config['upload_path'] = './img/columna/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '0';
        $config['file_name'] = $datos["Titulo"];

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $datos["Foto"] = $data["upload_data"]["file_name"];
            unset($datos["action"]);
            $datos["Fecha"] = date("Y-m-d");
            $this->madmin->nuevacolumna($datos);
            redirect("admin/dashboard","refresh");
        }
	}
//Fin metodo nuevacolumna

    public function nuevousuario(){
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->post()!=NULL){
                date_default_timezone_set('America/Mexico_City');
                $data["Fecha"] = date('Y-m-d H:i:s');
                $data["TipoUsuario"] = 1;
                $data["Password"] = sha1(md5($data["Password"]));
                $array = array("result" => $this->madmin->nuevousuario($data));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function getusuarios()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->get()!=NULL){
                $array = array("result" => $this->madmin->getusuarios());
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function getusuario()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->get()==NULL){
                $data = $this->madmin->getusuario($id);
                $array = array("Nombre" => $data["0"]["Nombre"],"Correo" => $data["0"]["Correo"]);
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function editarusuario()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->post()!=NULL){
                $array = array("result" => $this->madmin->editarusuario($id,$data));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function eliminarusuario()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()!=NULL){
                $array = array("result" => $this->madmin->eliminarusuario($id));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }

     public function nuevotag(){
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->post()!=NULL){
                $data["Fecha"] = date("Y-m-d");
                $data["idUsuario"] = $this->session->userdata('identificador');
                $array = array("result" => $this->madmin->nuevotag($data));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function gettags()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->get()==NULL){
                $array = array("result" => $this->madmin->gettags($id));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function gettag()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->get()==NULL){
                $data = $this->madmin->gettag($id);
                $array = array("Nombre" => $data["0"]["Nombre"],"Correo" => $data["0"]["Correo"]);
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function editartag()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->post()!=NULL){
                $array = array("result" => $this->madmin->editarusuario($id,$data));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }
    public function eliminartag()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()!=NULL){
                $array = array("result" => $this->madmin->eliminarusuario($id));
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }
        }
    }

	public function intercambio()
    {
        $data = $this->input->post();
        $array = array();
        array_push($array, $data["id"]);
        array_push($array, $data["idNext"]);
        if($data!=NULL){

                $id = substr($data["id"],strpos($data["id"],"-")+1);
                $idNext = substr($data["idNext"],strpos($data["idNext"],"-")+1);
                 array_push($array, $id);
                 array_push($array, $idNext);

                $idImportancia = $this->madmin->getImportancia($id);
                $idNextImportancia = $this->madmin->getImportancia($idNext);
                $idImportancia = $idImportancia["0"]["Importancia"];
                $idNextImportancia = $idNextImportancia["0"]["Importancia"];
                 array_push($array, $idImportancia);
                    array_push($array, $idNextImportancia);
                if(is_numeric($idImportancia)&&is_numeric($idNextImportancia)){
                    $this->madmin->setImportancia($id,$idNextImportancia);
                    $this->madmin->setImportancia($idNext,$idImportancia);

                    array_push($array, "SISIMON");
                }else{
                    //
                    array_push($array, "Error en numeros 2");
                }

        }
        $this->output->set_content_type('application/json')->set_output(json_encode($array));
    }

    public function nuevobanner()
    {
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->post()!=NULL){
                //Inicio AJAX/POST nuevobanner
                foreach ($data as $key => $value) {
                    $data["$key"] = $this->security->xss_clean($value);
                }
                $data["Habilitado"] = 1;

                $validation = $this->madmin->getbanner($data["Seccion"], $data["Posicion"]);

                if ($validation==FALSE) {

                	$data['IDUsuario']= $this->session->userdata('identificador');
	                $this->form_validation->set_rules('Cliente','Nombre del cliente','trim|required|max_length[50]'); 
                    $this->form_validation->set_rules('Enlace','Dirección del banner','trim|required|max_length[150]'); 

                    if(!is_dir("./img/banners/".$data["Seccion"]."/".$data["Posicion"]."/")){
                        mkdir("./img/banners/".$data["Seccion"]."/",0777);
                        mkdir("./img/banners/".$data["Seccion"]."/".$data["Posicion"]."/",0777);
                    }

                    	
			        $config['upload_path'] = "./img/banners/".$data["Seccion"]."/".$data["Posicion"]."/";
			        $config['allowed_types'] = 'jpg|png|gif';
			        $config['max_size']      = '0';
			        $config['overwrite']     = FALSE;
			        $config['encrypt_name']	 = TRUE;
                    


                    $this->load->library('upload', $config);
                    $data_img['file_name'] = "";
                    if($this->upload->do_upload('userfile')==FALSE){
                        $array = array("result" => $this->upload->display_errors());
                        
                    }else{
                    	$array = array("result" => "Completo");
                    	$data_img = $this->upload->data();
                    }
                    
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                	unset($data['userfile']);
                	if ($data_img['file_name'] != NULL) {
                        $data['ActivePicture'] = $data_img['file_name'];
                    }
                 	$this->madmin->nuevobanner($data);
                 	$data_new = array(
		                 				'Banner' => $data['ActivePicture'], 
		                 				'Seccion' => $data['Seccion'], 
		                 				'Posicion' => $data['Posicion']
		                 		);
                 	$this->madmin->recordsbanner($data_new);

                }else{
	                                       	
			        $config['upload_path'] = "./img/banners/".$data["Seccion"]."/".$data["Posicion"]."/";
			        $config['allowed_types'] = 'jpg|png|gif';
			        $config['max_size']      = '0';
			        $config['overwrite']     = FALSE;
			        $config['encrypt_name']	 = TRUE;
                    

                    $this->load->library('upload', $config);
                    
                    if($this->upload->do_upload('userfile')==FALSE){
                        $array = array("result" => $this->upload->display_errors());
                        

                    }else{
                    	$array = array("result" => "Completo");
                    	$data_img = $this->upload->data();
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                	unset($data['userfile']);
                    if ($data_img['file_name'] != NULL) {
                        $data['ActivePicture'] = $data_img['file_name'];
                        $datos = array('ActivePicture' => $data['ActivePicture']);
                    }
                	foreach ($validation as $row)
					{
					    $this->madmin->editarbanner($row['ID'], $datos);
					    $data_new = array(
             				'Banner' => $data['ActivePicture'], 
             				'Seccion' => $data['Seccion'], 
             				'Posicion' => $data['Posicion']
		                );
                 		$this->madmin->recordsbanner($data_new);
					}
                 	
            	}
            }
    }

    public function getbanners()
    {
        if($this->input->is_ajax_request()){
            if($this->input->post()==NULL){
                //Inicio AJAX/GET geteventos
                $array = $this->madmin->getbanners();
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
                //Fin AJAX/GET geteventos
            }
        }
    }

    //Inicio metodo getbanner (admin/banner)
    public function getbanner()
    {
        if($this->input->is_ajax_request()){
            $seccion = $this->uri->segment(3);
            $id = $this->uri->segment(4);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getbanner
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getbanner($seccion,$id);
                    $array = $array["0"];
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/GET getbanner
            }
        }
    }
    //Fin metodo getbanner

   

    //Inicio metodo eliminarbanner
    public function eliminarbanner()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->get()==NULL){
                //Inicio AJAX/POST eliminarbanner
                if(is_numeric($id)&&$id>0){
                    $return = $this->madmin->eliminarbanner($id);
                    $array  = array("result" => $return);
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));   
                }
                //Fin AJAX/POST eliminarbanner
            }
        }
    }
    //Fin metodo eliminarbanner
    //Inicio metodo fotoportada
    public function fotoportada()
    {
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
                //Inicio AJAX/POST nuevobanner
                foreach ($data as $key => $value) {
                    $data["$key"] = $this->security->xss_clean($value);
                }
                if($this->uri->segment(4)==1){
                    if(!is_dir("./img/portadas/estetica/")){
                            mkdir("./img/portadas/estetica/",0777);
                    }
                    $config['upload_path'] = "./img/portadas/estetica/";
                    $config['allowed_types'] = 'jpg|png|gif';
                    $config['max_size']      = '0';
                    $config['overwrite']     = FALSE;
                    $config['encrypt_name']  = TRUE;
                    


                    $this->load->library('upload', $config);
                    
                    if($this->upload->do_upload('userfile')==FALSE){
                        $array = array("result" => $this->upload->display_errors());
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                        
                    }else{
                        $data_img = $this->upload->data();
                        $array = array("result" => $data_img['file_name']);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }
                }elseif($this->uri->segment(4)==2){
                    if(!is_dir("./img/portadas/sustancial/")){
                        mkdir("./img/portadas/sustancial/",0777);
                    }
                    $config['upload_path'] = "./img/portadas/sustancial/";
                    $config['allowed_types'] = 'jpg|png|gif';
                    $config['max_size']      = '0';
                    $config['overwrite']     = FALSE;
                    $config['encrypt_name']  = TRUE;
                    


                    $this->load->library('upload', $config);
                    
                    if($this->upload->do_upload('userfile')==FALSE){
                        $array = array("result" => $this->upload->display_errors());
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                        
                    }else{
                        $data_img = $this->upload->data();
                        $array = array("result" => $data_img['file_name']);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }
                }      
    }

    //Inicio metodo editarevento
    public function nuevaportada()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->post()!=NULL){
                //Inicio AJAX/POST editarbanner
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }
                    $this->form_validation->set_rules('NombrePortada','Nombre','trim|required|max_length[50]');
                    $this->form_validation->set_rules('ImagenPortada1','Portada Estetica','trim|required|max_length[130]');
                    $this->form_validation->set_rules('ImagenPortada2','Portada Sustancial','trim|required|max_length[130]');
                    $s=explode(' ',$data['Fecha']);
                    $w=explode(',',$s[1]);
                    $mes =array(
                        'January'=>'01',
                        'February'=>'02',
                        'March'=>'03',
                        'April'=>'04',
                        'May'=>'05',
                        'June'=>'06',
                        'July'=>'07',
                        'August'=>'08',
                        'September'=>'09',
                        'October'=>'10',
                        'November'=>'11',
                        'December'=>'12'
                    );
                    foreach($mes as $month => $number){
                        if($w[0]==$month){
                            $value=$number;
                        }
                    }
                    $data['Fecha'] = $s[2]."-".$value."-".$s[0];

                    $array = array("result" => $this->madmin->nuevaportada($data));
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                //Fin AJAX/POST editarbanner
            }
        }
    }
    //Fin metodo editarevento

    public function getportadas()
    {
        if($this->input->is_ajax_request()){
            if($this->input->post()==NULL){
                //Inicio AJAX/GET geteventos
                $array = $this->madmin->getportadas();
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
                //Fin AJAX/GET geteventos
            }
        }
    }

    //Inicio metodo getbanner (admin/banner)
    public function getportada()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getbanner
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getportada($id);
                    $array = $array["0"];
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/GET getbanner
            }
        }
    }
    //Fin metodo getbanner

    //Inicio metodo editarevento
    public function editarportada()
    {
        if($this->input->is_ajax_request()){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            $id = $this->uri->segment(3);
            if($this->input->post()!=NULL){
                //Inicio AJAX/POST editarbanner
                if(is_numeric($id)&&$id>0){
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }
                    $this->form_validation->set_rules('NombrePortada','Nombre','trim|required|max_length[50]');
                    $this->form_validation->set_rules('ImagenPortada1','Portada Estetica','trim|required|max_length[130]');
                    $this->form_validation->set_rules('ImagenPortada2','Portada Sustancial','trim|required|max_length[130]');}

                    $array = array("result" => $this->madmin->editarportada($id,$data));
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/POST editarbanner
            }
        }
    //Fin metodo editarevento

    //Inicio metodo eliminarbanner
    public function eliminarportada()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->get()==NULL){
                //Inicio AJAX/POST eliminarbanner
                if(is_numeric($id)&&$id>0){
                    $return = $this->madmin->eliminarportada($id);
                    $array  = array("result" => $return);
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));   
                }
                //Fin AJAX/POST eliminarbanner
            }
        }
    }
    //Fin metodo eliminarbanner


}
