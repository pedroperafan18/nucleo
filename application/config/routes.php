<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["admin/evento/(:num)"] = "admin/getevento/$1";
$route["admin/seccion/(:num)"] = "admin/getseccion/$1";
$route["admin/video/(:num)"] = "admin/getvideo/$1";
$route["admin/revista/(:num)"] = "admin/getrevista/$1";
$route["admin/columna/(:num)"] = "admin/getcolumna/$1";
$route["admin/banner/(:num)"] = "admin/getbanner/$1";
$route["admin/usuario/(:num)"] = "admin/getusuario/$1";
$route["admin/tag/(:num)"] = "admin/gettag/$1";
$route["admin/portada/(:num)"] = "admin/getportada/$1";


$route["admin/evento/"] = "admin/geteventos";
$route["admin/seccion/"] = "admin/getsecciones";
$route["admin/video/"] = "admin/getvideos";
$route["admin/revista/"] = "admin/getrevistas";
$route["admin/columna/"] = "admin/getcolumnas";
$route["admin/banner/"] = "admin/getbanners";
$route["admin/usuario/"] = "admin/getusuarios";
$route["admin/tags/(:num)"] = "admin/gettags/$1";
$route["admin/portada/(:num)"] = "admin/getportadas";

$route["admin/evento/(:num)/editar"] = "admin/editarevento/$1";
$route["admin/seccion/(:num)/editar"] = "admin/editarseccion/$1";
$route["admin/video/(:num)/editar"] = "admin/editarvideo/$1";
$route["admin/revista/(:num)/editar"] = "admin/editarrevista/$1";
$route["admin/columna/(:num)/editar"] = "admin/editarcolumna/$1";
$route["admin/banner/(:num)/editar"] = "admin/editarbanner/$1";
$route["admin/usuario/(:num)/editar"] = "admin/editarusuario/$1";
$route["admin/tag/(:num)/editar"] = "admin/editartag/$1";
$route["admin/portada/(:num)/editar"] = "admin/editarportada/$1";

$route["admin/evento/(:num)/eliminar"] = "admin/eliminarevento/$1";
$route["admin/seccion/(:num)/eliminar"] = "admin/eliminarseccion/$1";
$route["admin/video/(:num)/eliminar"] = "admin/eliminarvideo/$1";
$route["admin/revista/(:num)/eliminar"] = "admin/eliminarrevista/$1";
$route["admin/columna/(:num)/eliminar"] = "admin/eliminarcolumna/$1";
$route["admin/banner/(:num)/eliminar"] = "admin/eliminarbanner/$1";
$route["admin/usuario/(:num)/eliminar"] = "admin/eliminarusuario/$1";
$route["admin/tag/(:num)/eliminar"] = "admin/eliminartag/$1";
$route["admin/portada/(:num)/eliminar"] = "admin/eliminarportada/$1";

$route["admin/evento/nuevo"] = "admin/nuevoevento/";
$route["admin/seccion/nuevo"] = "admin/nuevaseccion/";
$route["admin/video/nuevo"] = "admin/nuevovideo/";
$route["admin/revista/nuevo"] = "admin/nuevarevista/";
$route["admin/columna/nuevo"] = "admin/nuevacolumna/";
$route["admin/usuario/nuevo"] = "admin/nuevousuario/";
$route["admin/banner/nuevo"] = "admin/nuevobanner/";
$route["admin/tag/nuevo"] = "admin/nuevotag/";
$route["admin/portada/nuevo"] = "admin/nuevaportada/";
$route["admin/portada/foto/(:num)"] = "admin/fotoportada/$1";
$route["admin/evento/(:num)/fotos"] = "admin/getfotosevento/$1/fotos";
$route["admin/seccion/(:num)/fotos"] = "admin/getfotosseccion/$1/fotos";
$route["admin/foto/(:num)/eliminar"] = "admin/eliminarfoto/$1";
$route["admin/banner/(:num)/(:num)"] = "admin/getbanner/$1/$2";
