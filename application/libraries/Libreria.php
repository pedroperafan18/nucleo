<?php
class Libreria{
	public function get_seccion($e){$var="";
		switch($e){
            case 1: $var = "Look"; break;
            case 2: $var = "Viajero"; break;
            case 3: $var = "Con Gusto"; break;
            case 4: $var = "La Sesión"; break;
            case 5: $var = "Quién Soy"; break;
            case 6: $var = "Portada Sustancial"; break;
            case 7: $var = "Cocina De"; break;
            case 8: $var = "Acciones"; break;
            case 9: $var = "Salud"; break;
            case 10: $var = "Ciudadano Activo"; break;
            case 11: $var = "Figura"; break;
            case 12: $var = "Cultura"; break;
        }
        return $var;
	}

    
    public function fecha($fecha){
       date_default_timezone_set('America/Mexico_City');
       $f0 = strtotime(date("Y-m-d H:i:s"));
       $f1 = strtotime($fecha);
       $seg = ($f0 - $f1);
       if($seg > 60){
       $min = $seg / 60; 
       if($min > 60){
       $hor = $min / 60; 
       if($hor > 24){
       $dia = $hor / 24;
       if($dia > 7 ){
       $dias = substr($fecha, 8, -8);
       $anos = substr($fecha, 0, 4);
       $mess = substr($fecha, 5, 6);
       $hora = substr($fecha, 11, -3);
       switch($mess){
          case 1:$mess = 'Enero';break;
          case 2:$mess = 'Febrero';break;
          case 3:$mess = 'Marzo';break;
          case 4:$mess = 'Abril';break;
          case 5:$mess = 'Mayo';break;
          case 6:$mess = 'Junio';break;
          case 7:$mess = 'Julio';break;
          case 8:$mess = 'Agosto';break;
          case 9:$mess = 'Septiembre';break;
          case 10:$mess = 'Octubre';break;
          case 11:$mess = 'Noviembre';break;
          case 12:$mess = 'Diciembre';break;
       }
      $resultado =  $dias.' de '.$mess.' del '.$anos.' a la(s) '.$hora;
       }else{
       $resultado = round($dia);
       if($resultado==1){$resultado = 'Hace aproximadamente un d&iacute;a';}else{$resultado = 'Hace aproximadamente '.$resultado.' d&iacute;as';}
       }
       }else{
       $resultado = round($hor);
       if($resultado==1){$resultado = 'Hace aproximadamente una hora';}else{$resultado = 'Hace aproximadamente '.$resultado.' horas';}
       }
       }else{
        $resultado = round($min);
        if($resultado==1){$resultado = 'Hace aproximadamente un minuto';}else{$resultado = 'Hace aproximadamente '.$resultado.' minutos';}
       }
       }else{
       $resultado = 'Hace '.$seg.' segundos';
       }
    
       if(isset($resultado))
        return $resultado;
    }
// CONFIGURACION DE SUBIDA DE MULTIPLES IMAGENES

    public function set_upload_options($e){
        $config = array();
        $config['upload_path'] = $e;
        $config['allowed_types'] = 'jpg|png|gif';
        $config['max_size']      = '2000';
        $config['overwrite']     = FALSE;
        $config['encrypt_name']  = TRUE;
        return $config;
    }
}
?>