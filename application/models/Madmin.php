<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Madmin extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function geteventos()
	{
		$this->db->where('Habilitado',"1");
        $this->db->order_by("Importancia", "asc"); 
		$query=$this->db->get('eventos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getevento($id)
	{
		$this->db->where('IDEvento', $id);
		$query = $this->db->get('fotografiaseventos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function geteventofotos($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('eventos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function eliminarfotoevento($id){
		$data = array('Habilitado' => '0');
		$this->db->where('ID',$id);
		if($this->db->update('fotografiaseventos', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function editarevento($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('eventos', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarevento($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('eventos', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevoevento($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('eventos',$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function getvideos()
	{
		$this->db->where('Habilitado', "1");
        $this->db->order_by("ID", "desc"); 
		$query=$this->db->get('videos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getvideo($id)
	{
		$this->db->where('Habilitado', "1");
        $query=$this->db->get('videos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarvideo($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('videos', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarvideo($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('videos', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevovideo($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('videos',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}
	public function getsecciones()
	{
		$this->db->where('Habilitado', "1");
        $this->db->order_by("ID", "desc"); 
		$query=$this->db->get('secciones');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getseccion($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('secciones');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarseccion($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('secciones', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarseccion($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('secciones', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevaseccion($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('secciones',$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	public function getfotosseccion($id){
		$this->db->where('IDSeccion', $id);
		$query = $this->db->get('imagenessecciones');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getrevistas()
	{
		$this->db->where('Habilitado', "1");
        $this->db->order_by("ID", "desc"); 
		$query=$this->db->get('impresa');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getrevista($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('impresa');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarrevista($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('impresa', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarrevista($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('impresa', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevarevista($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('impresa',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}
	public function getcolumnas()
	{
		$this->db->where('Habilitado', "1");
        $this->db->order_by("ID", "desc"); 
		$query=$this->db->get('columnas');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getcolumna($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('columnas');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarcolumna($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('columnas', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarcolumna($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('columnas', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevacolumna($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('columnas',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}


	public function getusuarios()
	{
		$this->db->where('Habilitado', "1");
        $this->db->order_by("ID", "desc"); 
		$query=$this->db->get('usuario');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getusuario($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$this->db->select('ID,Nombre,Correo,Fecha');
		$query = $this->db->get('usuario');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarusuario($id,$data)
	{
		$this->db->where('Habilitado', "1");
		$this->db->where('ID', $id);
		if($this->db->update('usuario', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarusuario($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('usuario', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function nuevousuario($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('usuario',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}

	public function gettags($id)
	{
		$this->db->where('Habilitado', "1");
		$this->db->where('IDEvento', $id);
        $this->db->order_by("ID", "desc"); 
		$query=$this->db->get('tags');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function gettag($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$this->db->select('ID,NombrePersona,Fecha');
		$query = $this->db->get('tags');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editartag($id,$data)
	{
		$this->db->where('Habilitado', "1");
		$this->db->where('ID', $id);
		if($this->db->update('tags', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminartag($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('tags', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function nuevotag($data)
	{
		$data["Habilitado"] = 1;
		if($this->db->insert('tags',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}

	public function login($data){
		$this->db->where('Correo',$data['Correo']);
		$this->db->where('Password',$data['Password']);
		$this->db->where('TipoCuenta','1');
		$query = $this->db->get('usuario');
		if ($query->num_rows() > 0){
          return $query->result_array();
        } else {
          return FALSE;
        }
	}

	public function asignarImportanciaInicial($id)
    {
    	$data = array(
            'Importancia'=> $id,
        );  
        $this->db->where("ID", $id);
        $this->db->update('eventos', $data);
    }

    // INSERTA IMAGENES DEL BLOG
    public function insertarImagenSeccion($data){
		return $this->db->insert('imagenessecciones',$data);
    }
    public function insertarimagenevento($data){
    	return $this->db->insert('fotografiaseventos',$data);
    }

    public function getImportancia($id){
		$this->db->select('Importancia');
		$this->db->where('ID', $id);
		$query = $this->db->get('eventos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return NULL;
		}
	}
	public function setImportancia($id,$importancia)
	{
		$this->db->where('ID', $id);
		$object = array(
			'Importancia'=>$importancia
		);
		$this->db->update('eventos', $object);
	}

	public function nuevobanner($data)
	{
		if($this->db->insert('banners',$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function recordsbanner($data)
	{
		if($this->db->insert('bannersrecord',$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function getbanners()
	{
		$query=$this->db->get('banners');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function getbanner($seccion,$id)
	{
		$this->db->where('Seccion',$seccion);
		$this->db->where('Posicion',$id);
		$query = $this->db->get('banners');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function show_banner($seccion)
	{
		$this->db->where('Seccion',$seccion);
		$query = $this->db->get('banners');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

    public function editarbanner($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('banners', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function eliminarbanner($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('banners', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevaportada($data)
	{
		if($this->db->insert('portada',$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	public function getNombresSecciones()
	{
		$this->db->where("Habilitado","1");
		$query=$this->db->get('nombresecciones');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getportadas()
	{
		$this->db->where("Habilitado",1);
		$query=$this->db->get('portada');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getNombreSeccion($id)
	{
		$this->db->where("Habilitado","1");
		$this->db->where("ID",$id);
		$query=$this->db->get('nombresecciones');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getportada($id)
	{
		$this->db->where('ID',$id);
		$query = $this->db->get('portada');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function nuevoNombreSeccion($data)
	{
		$data["Habilitado"] = "1";
		if($this->db->insert('nombresecciones',$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
    public function editarportada($id,$data)
	{
		$this->db->where('ID', $id);
		if($this->db->update('portada', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function editarNombreSeccion($data,$id)
	{
		$this->db->where('ID', $id);
		if($this->db->update('nombresecciones', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarportada($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('portada', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarNombreSeccion($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('nombresecciones', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
//////////////////<


//////////////////>
}
